# Wear – Wired Sensor Device

Repository containing the design files of the Wired Sensor device used on the Wear project.

To get a Sensor Device already assembled, please contact the Hardware Platform from the Champalimaud Foundation using this [e-mail](mailto:hardware@neuro.fchampalimaud.org). 

![WiredSensor](Documents/ImagesAndPics/WiredSensor.jpg)

## What is the Wired Sensor device?

It’s an ultra-lightweight 9-axis motion sensor with electrostimulation capabilities.

The main features are:

* Weight less than 200 mg
* Configurable sample rate up to 1 KHz (when connected to the Wear Basestation)
* Stimulation current up to 500 mA (when connected to the Wear Basestation)
* Configurable ranges (when connected to the Wear Basestation)

## What can I find on this repository?

* Design files of the Printed Circuit Board (PCB)
* The Build Of Materials
* A panel PCB that can be used for production using a pick and place
* A folder **Documents** containing files used for the design, datasheets and technical notes

## What software do I need to view or open the files?

* PCB files were designed using [Eagle 6.5.0](https://bitbucket.org/fchampalimaud/corelibrary.atxmega/downloads/Eagle-Win-6.5.0.exe).
* To view the Gerber files from the Downloads page, you can use an online Gerber Viewer like the [one](http://mayhewlabs.com/3dpcb) from Mayhew Labs or a standalone Open Source Gerber Viewer like [this](http://gerbv.geda-project.org/) old one that fits the purpose.

## What file .gitignore should I use?

When you clone, fork or branch this repository, you are encouraged to create a file named **.gitignore** on your local repository root to ensure that you are not sending unnecessary files to the repository. For this repository, it is recommended a **.gitignore** file with the following content:

    desktop.ini
    **/Downloads
    **/downloads

## How do I get set up? ###

### Build the Sensor Device

1. Manufacture the PCB or Panel using 0.5 mm as thickness.
2. Assemble the components listed on the Build Of Materials.
3. **Note:** Due to the small components we recommend to outsource the entire process.

### Build the cable (to connect the Wired Device to the Wear Basestation)

1. Use any Polarized Nano Connector from [Omnetics](http://www.omnetics.com/) that fits into connector A79613-001, like the A79614-001 or the A79610-001 (with premounted wires).
2. **Note:** To achieve low weight, you should use wires with a high AWG (36 or more).

### Test Sensor Device and cable
1. Plug the cable into the Wired Sensor and into the Wear Basestation.
2. You should have a Wear Basestation already working before run this test. For more information go [here](https://bitbucket.org/fchampalimaud/device.wear.basestation).
3. Chose the Wired Sensor and Start an Acquisition. The motion indicators will show the data being collected.

## Who do I talk to?

The Wear project is maintained by the [Hardware Platform team](http://www.cf-hw.org) from the [Champalimaud Foundation](http://neuro.fchampalimaud.org/). For suggestions and doubts, please use this [e-mail](mailto:hardware@neuro.fchampalimaud.org).

**Note:** The Wired Sensor already **assembled**, **tested** and **ready to use** can be provided by the Hardware Platform.

## Links

*To be added!*